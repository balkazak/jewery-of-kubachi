// npm i gulp gulp-less gulp-autoprefixer gulp-group-css-media-queries browser-sync

var gulp         = require('gulp');
var less         = require('gulp-less');
var autoprefixer = require('gulp-autoprefixer');
var gcmq         = require('gulp-group-css-media-queries');
var browserSync  = require('browser-sync').create();

var config = {
    src: './src/',
    css: {
        watch: 'less/**/*.less',
        src: 'less/style.less',
        dest: 'css'
    },
    html: {
        src: '*.html'
    },
    js: {
        src: '*.js'
    }
};

gulp.task('pack', function(){
    gulp.src(config.src + config.css.dest + '/**/*.css')
        .pipe(autoprefixer({
            browsers: ['> 1%'],
            cascade: false
        }))
       .pipe(gulp.dest(config.src + config.css.dest));
});

gulp.task('build', function(){
   gulp.src(config.src + config.css.src)
       .pipe(less())
       .pipe(gcmq())
       .pipe(gulp.dest(config.src + config.css.dest))
       .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('watch', ['browserSync'], function(){
    gulp.watch(config.src + config.css.watch, ['build']);
    gulp.watch(config.src + config.js.src, browserSync.reload);
    gulp.watch(config.src + config.html.src, browserSync.reload);
});

gulp.task('browserSync', function(){
    browserSync.init({
        server: {
            baseDir: "./src"
        }
    });
});
