jQuery(document).ready(function($) {

	// Menu
	$('.header__menu-toggler').on('click', function(event) {
		event.preventDefault();

		$('.header .menu').toggleClass('menu_active', function() {

			if ( $(this).css('display') === 'none') {
				$(this).removeAttr('style');
			}
		});
	});

	// Slider on Homepage

	$('.slider').slick({
		slidesToShow: 1,
		dots: true,
		speed: 1000,
	});


	$('.slider-product').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      dots: true,
      asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      asNavFor: '.slider-product',
      dots: true,
      centerMode: true,
      focusOnSelect: true
    });

	// Slidet dots width
	function alignDots() {
		var dots = $('.home .slick-dots');
		var container = dots.closest('.carousel').find('.container');

		// dots.width() = container.width();

		console.log( container.width() );
	}

	alignDots();

	// cart item delete
	$('.cart_delete').on('click', function(event) {
		event.preventDefault();
		
		$(this).closest('.cart__item').remove();
	});

	// cart item increment
	$('.cart__counter').on('click', '.cart__q_inc', function(event) {
		event.preventDefault();

		var quantity = $(this).siblings('.cart__quantity');
		var value 	 = parseInt( quantity.html(), 10 ) + 1;

		quantity.text( value );
	});

	// cart item decrement
	$('.cart__q_dec').on('click', function(event) {
		event.preventDefault();
		
		var quantity = $(this).siblings('.cart__quantity');
		var value 	 = parseInt( quantity.html(), 10 ) - 1;

		if ( value < 1 ) {
			value = 1;
		}

		quantity.text( value );
	});

	// Clear cart
	$('.cart__link_clear').on('click', function(event) {
		event.preventDefault();

		$('.cart__item').each(function(index, el) {
			el.remove();
		});
	});

	// Format number
	function formatNumber(num) {
	    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
	}

	$('.cart__item').each(function(index, el) {
		var priceNum    = $(this).find('.cart__sum span').text();
		var formatedNum = formatNumber( priceNum );

		$(this).find('.cart__sum span').text( formatedNum );
	});

	var formatedNumTotal = formatNumber( $('.cart__total-sum span').text() );
	$('.cart__total-sum span').text(formatedNumTotal);

	// tabs
	$('.tabs__title').on('click', function(event) {
		event.preventDefault();

		var dataAttr = $(this).data('content');

		if ( !$(this).hasClass('active') ) {
			$(this).addClass('active');
		}

		$(this).siblings().removeClass('active');

		$('.tabs__content').each(function(index, el) {
			$( this ).removeClass('active');

			if ( $( this ).data('content') === dataAttr ) {
				$( this ).addClass('active');
			}

			console.log($( this ));
		});
		

	});


});
